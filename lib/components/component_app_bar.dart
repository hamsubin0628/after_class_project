import 'package:after_class_project/config/config_color.dart';
import 'package:after_class_project/config/config_text.dart';
import 'package:flutter/material.dart';

class ComponentAppBar extends StatelessWidget {
  const ComponentAppBar({
    super.key,
    required this.text,
    required this.page
  });

  final String text;
  final String page;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pushNamed('${page}');
          },
          icon:Icon(Icons.keyboard_backspace,color: colorMainText,),
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          text,
          style: TextStyle(
            fontFamily: 'NotoSans_Bold',
            fontSize: appbarTitle,
            color: colorPrimary,
            letterSpacing: textLetterSpacing,
          ),
        ),
      ),
    );
  }
}
