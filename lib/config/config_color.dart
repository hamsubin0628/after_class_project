import 'package:flutter/material.dart';

const Color colorMainText = Color.fromRGBO(64, 64, 64, 1);
const Color colorPrimary = Color.fromRGBO(52, 152, 219, 1); /// 파란색 전체
const Color colorLightBlue = Color.fromRGBO(52, 152, 219, 35); /// 연한 파란색
const Color colorLightText = Color.fromRGBO(190, 190, 190, 1);
const Color colorLightBtn = Color.fromRGBO(230, 230, 230, 1);