/// 텍스트 사이즈
const double appbarTitle = 20;
const double textSuper = 20;
const double textL = 17;
const double textM = 15;
const double textS = 13;

/// 텍스트 자간
const double textLetterSpacing = -0.5;