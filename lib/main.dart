import 'package:after_class_project/pages/page_index.dart';
import 'package:after_class_project/pages/page_join.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: "/page_join",
      routes: {
        "/page_index":(context) => PageIndex(),
        "/page_join":(context) => PageJoin(),
      },
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blueAccent),
          fontFamily: "NotoSansKR"
      ),
    );
  }
}