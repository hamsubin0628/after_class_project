import 'package:after_class_project/components/component_app_bar.dart';
import 'package:after_class_project/config/config_color.dart';
import 'package:after_class_project/config/config_form_validator.dart';
import 'package:after_class_project/config/config_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageJoin extends StatefulWidget {
  const PageJoin({super.key});

  @override
  State<PageJoin> createState() => _PageJoinState();
}

class _PageJoinState extends State<PageJoin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(45),
        child: ComponentAppBar(
          text: '회원가입',
          page: '/page_login',
        ),
      ),
      body: SingleChildScrollView(
        /// 전체 감싸는 틀
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: [
              Container(
                child: Column(
                  children: [
                    Container(
                      /// 아이디 *
                      child: Row(
                        children: [
                          Text(
                              '아이디',
                            style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: textM,
                              color: colorMainText,
                              letterSpacing: textLetterSpacing,
                            ),
                          ),
                          Text(
                              ' *',
                            style: TextStyle(
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: textM,
                              color: colorPrimary,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 45,
                      /// 아이디 입력한 & 중복확인 버튼
                      child: Row(
                        children: [
                          // FormBuilderTextField(
                          //   name: '아이디',
                          //   validator: FormBuilderValidators.compose([
                          //     FormBuilderValidators.required(errorText: formErrorRequired), /** 필수 **/
                          //   ]),
                          //   /// 커서 색상
                          //   cursorColor: primaryColor,
                          //   /// 키보드 바깥 영역 탭할 시 키보드 닫기
                          //   onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                          //   keyboardType: TextInputType.text, /// 키보드 기본 텍스트 키 올라오게 하기
                          //   decoration:InputDecoration(
                          //     enabledBorder: UnderlineInputBorder( /// 기본 텍스트 필드 인풋 라인 색상
                          //       borderSide: BorderSide(color: primaryColor),
                          //     ),
                          //     focusedBorder: UnderlineInputBorder( /// 선택됐을 시 텍스트 필드 인풋 라인 색상
                          //       borderSide: BorderSide(color: Colors.orangeAccent),
                          //     ),
                          //     hintText: '* 아이디를 입력해 주세요.',
                          //     hintStyle: TextStyle(
                          //         fontSize: textM,
                          //         fontFamily:'NotoSans_NotoSansKR-Regular',
                          //         color: lightTextColor,
                          //         letterSpacing: -0.5
                          //     ),
                          //   ),
                          //   style: TextStyle(
                          //       fontSize: textM,
                          //       fontFamily:'NotoSans_NotoSansKR-Regular',
                          //       color: mainTextColor,
                          //       letterSpacing: -0.5
                          //   ),
                          // ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
